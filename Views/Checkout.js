import React from 'react'
import { BackHandler, View, TouchableOpacity, Platform, Text, TextInput, StyleSheet, Alert, WebView } from 'react-native'
import * as Icon from '@expo/vector-icons'
import TopNavigation from '../Components/TopNavigation'
import { withNavigation } from 'react-navigation'
import { connect } from 'react-redux'
import { isEmailAddress } from '../utils';
import { postOrder } from '../redux/actions/tickets'


//TODO:
//import { WebBrowser } from 'expo'

class Checkout extends React.Component {

	state = {
		quantity: 1,
		name: '',
		email: '',
		checkbox: false,
		validationErrors: {email: '', name: '', checkbox: ''}
	}

	componentDidMount () {
		BackHandler.addEventListener('hardwareBackPress', this.backPressHandler)
	};

	componentWillUnmount = () => {
		BackHandler.removeEventListener('hardwareBackPress', this.backPressHandler)
	}

	backPressHandler = () => {
		this.props.navigation.navigate('Home')
		return true
	}
	handlePaymentStatus = (message) => {
		console.log(message)
		if (message === 'closeWindow') {
			this.props.navigation.navigate('Checkout')
		}
	}
	openWebPage = (paymentUrl) => {
		return (
			<WebView
				source={{uri: paymentUrl}}
				onMessage={(event)=>{
					let message  = event.nativeEvent.data;
					console.log(message)
					console.log('....')
					this.handlePaymentStatus(message)
				}}
				useWebKit={true}
				style={{marginTop: 20}}
			/>
		)
	}
	setTicket = (quantity) => {
		if (quantity < 200) {
			this.setState({quantity: quantity < 1 ? 1 : Number(quantity)})
		}
	}
	addTicket = (add) => {
		if (add) {
			this.setState({quantity: this.state.quantity + 1})
		} else {
			if (this.state.quantity > 1) {
				this.setState({quantity: this.state.quantity - 1})
			}
		}
	}

	handleSubmit = () => {
		const {quantity, name, checkbox, email} = this.state
		const {product} = this.props

		let validationErrors = {};
		if (email === '') { validationErrors.email = 'Adres e-mail jest wymagany!'; }
		else { validationErrors.email = isEmailAddress(email) ? '' : 'Niepoprawny adres e-mail!'; }
		if (name === '') { validationErrors.name = 'Imię jest wymagane!'; }
		else { validationErrors.name = name.length > 2 ? '' : 'Imię jest za krótkie!'; }
		if (!checkbox) { validationErrors.checkbox = 'Akceptacja regulaminu jest wymagana!'; }
		if (quantity < 1) { validationErrors.quantity = 'Minimalna ilość biletów powinna być większa niż 0!'; }

		if (!validationErrors.email && !validationErrors.name && !validationErrors.name && !validationErrors.quantity) {
			const order = {
				email: email,
				quantity: quantity,
				product_id: product.id
			}
			this.props.dispatch(postOrder(order));
		} else {
			this.setState({
				validationErrors: validationErrors
			}, () => this.showAlert())
		}
	}

	showAlert = () =>{
		let msgs = this.state.validationErrors
		msgs = Object.values(msgs).filter((item) => item !== "");
		Alert.alert(
			msgs[0]
		)
	}

	render () {
		const {quantity, checkbox} = this.state
		const {product, loading, paymentUrl} = this.props

		if (paymentUrl) {
			return this.openWebPage(paymentUrl);
		}

		if (loading) {
			return (
				<View style={{
					flex: 1,
					flexDirection: 'column',
					justifyContent: 'center',
					alignItems: 'stretch'
				}}>
						<Text style={styles.statisticText}>Loading...</Text>
				</View>
			)
		}

		return (
			<View style={{flex: 1, padding: 10}}>
				<TopNavigation title="Podaruj Bilet"/>
				<View style={{flex: 1}}>
					<Text style={styles.statisticText}>{quantity} x {product.price}zł = {quantity * product.price}zł</Text>
				</View>
				<View style={{
					flex: 1,
					flexDirection: 'row',
					justifyContent: 'space-evenly'
				}}>
					<TouchableOpacity
						style={styles.buyButton}
						onPress={() => this.addTicket(false)}>
						<Text style={styles.buyButtonText}>-</Text>
					</TouchableOpacity>
					<View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.15)'}}>
						<TextInput
							style={{flex: 1, fontSize: 20, textAlign: 'center', color: '#fff'}}
							placeholder="0"
							keyboardType='numeric'
							value={this.state.quantity.toString()}
							maxLength={999}
							onChangeText={(quantity) => this.setTicket(quantity)}
						/>
					</View>
					<TouchableOpacity
						style={styles.buyButton}
						onPress={() => this.addTicket(true)}>
						<Text style={styles.buyButtonText}>+</Text>
					</TouchableOpacity>
				</View>
				<View style={{flex: 9, padding: 10}}>
					<View style={{flex: 2, justifyContent: 'flex-end'}}>

						<Text style={styles.label}>Email*</Text>
						<View style={styles.input}>
							<TextInput
								style={{height: 40, color: '#fff'}}
								keyboardType={'email-address'}
								value={this.state.email}
								onChangeText={(email) => this.setState({email})}
							/></View>
						<Text style={styles.label}>Imię*</Text>
						<View style={styles.input}>
							<TextInput
								style={{height: 40, color: '#fff'}}
								value={this.state.name}
								onChangeText={(name) => this.setState({name})}
							/>
						</View>
					</View>
					<View style={{flex: 1, justifyContent: 'center'}}>
						<TouchableOpacity
							style={{marginTop: 50, flexDirection: 'row'}}
							onPress={()=>{this.setState({checkbox: !checkbox})}}
						>
							<View style={{flex: 1, justifyContent: 'center'}}>
								{!checkbox ?
									<Icon.Ionicons
										name={Platform.OS === 'ios' ? 'ios-square-outline' : 'md-square-outline'}
										size={18}
										style={{paddingRight: 0}}
										color="#fff"
									/>
									:
									<Icon.Ionicons
										name={Platform.OS === 'ios' ? 'ios-checkbox' : 'md-checkbox'}
										size={18}
										style={{paddingRight: 0}}
										color="#fff"
									/>
								}
							</View>
							<View style={{flex: 10}}>
								<Text style={{color: '#fff', fontSize: 12}}>Przeczytałem i zgadzam się na regulamin serwisu Akcja Bilet
									zamieszczony pod
									tym linkiem</Text>
							</View>
						</TouchableOpacity>
					</View>
					<View style={{flex: 1, justifyContent: 'flex-end', paddingBottom: 30}}>
						<TouchableOpacity
							style={[!checkbox || !this.state.name || !this.state.email ? styles.submitButtonDisabled : styles.submitButton, {marginTop: 50}]}
							onPress={() => this.handleSubmit()}
							disabled={!checkbox}
						>
							<Text style={styles.submitButtonText}>Zapłać teraz</Text>
						</TouchableOpacity>
					</View>

				</View>
			</View>
		)
	}
}

const mapStateToProps = (state) => ({
	product: state.checkout.product,
	paymentUrl: state.checkout.paymentUrl,
	loading: state.checkout.loading
})

const CheckoutWrapper = withNavigation(Checkout);

export default connect(mapStateToProps)(CheckoutWrapper)




const styles = StyleSheet.create({
	label: {
		color: '#fff',
		fontSize: 20,
		marginVertical: 10
	},
	statisticText: {
		textAlign: 'center',
		color: '#fff',
		fontSize: 22,
	},
	buyButton: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#e64f18',
		borderRadius: 6,
		shadowOpacity: 0.5,
		shadowRadius: 5,
		shadowColor: 'rgba(0,0,0,0.3)',
		shadowOffset: {height: 10, width: 0},
	},
	buyButtonText: {
		color: '#fff',
		fontSize: 30,
		fontWeight: 'bold'
	},
	submitButton: {
		backgroundColor: '#e64f18',
		borderRadius: 6,
		shadowOpacity: 0.5,
		shadowRadius: 5,
		shadowColor: 'rgba(0,0,0,0.3)',
		shadowOffset: {height: 10, width: 0},
	},
	submitButtonDisabled: {
		backgroundColor: 'rgba(0,0,0,0.15)',
		borderRadius: 6,
		shadowOpacity: 0.5,
		shadowRadius: 5,
		shadowColor: 'rgba(0,0,0,0.3)',
		shadowOffset: {height: 10, width: 0},
	},
	submitButtonText: {
		textAlign: 'center',
		color: '#fff',
		fontSize: 20,
		padding: 15
	},
	input: {
		backgroundColor: 'rgba(0,0,0,0.15)',
		paddingLeft: 12
	}
})
