import React from 'react'
import { TouchableOpacity, View } from 'react-native'
import { connect } from 'react-redux'
import Header from '../Components/Header'
import Kids from '../Components/Animations/Kids'
import Background from '../Components/Animations/Background'
import BrandHero from '../Components/Animations/BrandHero'
import Footer from '../Components/Footer'

class Home extends React.Component {

	componentDidMount () {
		setTimeout(() => {
			if (this.props.firstStart) {
				this.startScreen()
			}
		}, 3000)
	}


	startScreen = () => {
		this.props.disableStartScreen()
		this.refs.background.startScreen()
		this.refs.brandHero.startScreen()
		setTimeout(() => {
			this.refs.kids.startScreen()
		}, 200)
	}

	render () {
		let {firstStart} = this.props
		return (
			<TouchableOpacity onPress={this.startScreen}
												style={{
													flex: 1,
													flexDirection: 'column',
													justifyContent: 'space-between',
													alignItems: 'stretch',
												}}
												disabled={!firstStart}
												activeOpacity={1}>
				<View style={{flex: 3}}>
					<Header isFirstStart={firstStart}/>
				</View>
				<View style={{
					flex: 4
				}}>
					<View>

					<Background
						ref="background"
						isFirstStart={firstStart}/>
					<Kids
						ref="kids"
						isFirstStart={firstStart}/>
					<BrandHero
						ref="brandHero"
						isFirstStart={firstStart}/>
					</View>
				</View>
				<View style={{flex: 3, justifyContent: 'flex-end'}}>
					<Footer isVisible={firstStart}/>
				</View>
			</TouchableOpacity>
		)

	}
}

const mapStateToProps = (state) => ({
	firstStart: state.layout.firstStart
})

const mapDispatchToProps = (dispatch) => {
	return {
		disableStartScreen: () => dispatch({type: 'FIRST_START'})
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)

