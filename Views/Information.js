import React from 'react'
import { BackHandler, View, Platform, Text, StyleSheet, ScrollView } from 'react-native'
import * as Icon from '@expo/vector-icons'
import TopNavigation from '../Components/TopNavigation'


export default class Information extends React.Component {

	componentDidMount () {
		BackHandler.addEventListener('hardwareBackPress', this.backPressHandler)
	};

	componentWillUnmount = () => {
		BackHandler.removeEventListener('hardwareBackPress', this.backPressHandler)
	}

	backPressHandler = () => {
		const {navigate} = this.props.navigation
		navigate('Home')
		return true
	}

	render () {

		return (
			<View style={{flex: 1}}>
				<TopNavigation title="Information"/>
				<ScrollView style={{flex: 1,
					paddingHorizontal: 20,
				}}>
					<Text style={styles.head}>
						ORGANIZATORKI AKCJI BILET
					</Text>
					<View style={{flexDirection:'row'}}>
						<View style={styles.person}>
							<Icon.Ionicons
								name={Platform.OS === 'ios' ? 'ios-person' : 'md-person'}
								size={30}
								color="#fff"
							/>
							<Text style={styles.name}>Agata</Text>
							<Text style={styles.p}>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							</Text>
						</View>
						<View style={styles.person}>
							<Icon.Ionicons
								name={Platform.OS === 'ios' ? 'ios-person' : 'md-person'}
								size={30}
								color="#fff"
							/>
							<Text style={styles.name}>Magda</Text>
							<Text style={styles.p}>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							</Text>
						</View>
					</View>
					<Text style={styles.head}>
						JAK TO SIĘ ZACZEŁO?
					</Text>
					<Text style={styles.p}>
						Kochani,{"\n"}
						Od 5 lat razem z Wami spełniamy filmowe marzenia dzieci z domów dziecka.{"\n"}{"\n"}

						Zaczęło się w 2013 roku, kiedy zaprosiłyśmy naszych znajomych do wspólnej zbiórki pieniędzy. Naszym celem było przekazanie biletów do kina dla dzieci z Domu Dziecka Tęcza i Opoka.{"\n"}
						Ze względu na wielkie i z roku na rok rosnące wsparcie setek osób, udało nam się przekazać bilety do kina dla dzieci z różnych placówek opiekuńczych, takich jak wiejskie świetlice środowiskowe i rodzinne domy dziecka.{"\n"}{"\n"}

						Ważne dla nas jest to, żeby dzieci, które mają najtrudniejszą sytuacja materialną i rodzinną miały taki sam dostęp do kultury jak ich rówieśnicy.

						Jeśli macie ochotę wziąć udział w SZÓSTEJ AKCJI BILET - zapraszamy~{"\n"}{"\n"}

						CENA JEDNEGO ZAPROSZENIA TO 15 zł{"\n"}
						Sami decydujecie ile biletów chcecie przekazać{"\n"}{"\n"}

						[Dzieci będą mogły chodzić do kina lub teatru przez cały rok i same zdecydują, co chcą zobaczyć.]

					</Text>
					<View style={{height: 30}}></View>
				</ScrollView>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	head: {
		fontWeight: 'bold',
		color: '#fff',
		paddingVertical: 20,
		fontSize: 20
	},
	name: {
		fontWeight: 'bold',
		color: '#fff',
		paddingVertical: 10,
		fontSize: 18
	},
	p: {
		color: '#fff',
	},
	person: {
		flex:1,
		alignItems: 'center',
	}
})
