import React from 'react'
import { BackHandler, View, Platform, Text, StyleSheet } from 'react-native'
import TopNavigation from '../Components/TopNavigation'

export default class ThankYouPage extends React.Component {

	componentDidMount () {
		BackHandler.addEventListener('hardwareBackPress', this.backPressHandler)
	};

	componentWillUnmount = () => {
		BackHandler.removeEventListener('hardwareBackPress', this.backPressHandler)
	}

	backPressHandler = () => {
		const {navigate} = this.props.navigation
		navigate('Home')
		return true
	}

	render () {

		return (
			<View style={{flex: 1}}>
				<TopNavigation title="Information"/>
				<Text style={styles.head}>
					Dziękuje za dokonanie płatności!
				</Text>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	head: {
		fontWeight: 'bold',
		color: '#fff',
		paddingVertical: 20,
		fontSize: 20
	}
})
