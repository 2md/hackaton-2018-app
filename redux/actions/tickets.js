import actionTypes from './actionTypes';
import {dispatchWooCommerceRequest, dispatchWordpressRequest} from './dispatchers';

export const getTickets = () => {
	const url = 'products?status=publish';
	const action = actionTypes.GET_PRODUCTS;
	return dispatch => dispatchWooCommerceRequest({ dispatch, url, action });
};

export const postOrder = (order) => {
	const data = {
		"payment_method": "payu",
		"set_paid": false,
		"billing": {
			"email": order.email,
		},
		"line_items": [
			{
				"product_id": order.product_id,
				"quantity": order.quantity
			}
		],
		"shipping_lines": [
			{
				"method_id": "flat_rate",
				"method_title": "Flat Rate",
				"total": "0"
			}
		]
	}
	const url = 'orders';
	const action = actionTypes.POST_ORDER;
	const method = 'POST';
	return dispatch => dispatchWooCommerceRequest({ dispatch, url, action, method, data }).then(response => {
		dispatch(getPaymentUrl(response.payload.id));
	});
};


export const getPaymentUrl = (orderId) => {
	const url = `process-payment-order/${orderId}`;
	const action = actionTypes.GET_PAYMENT_URL;
	return dispatch => dispatchWordpressRequest({ dispatch, url, action });
};
