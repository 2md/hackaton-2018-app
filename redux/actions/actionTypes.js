const actionTypes = {};

const actions = [
	'GET_PRODUCTS',
	'POST_ORDER',
	'GET_PAYMENT_URL'
];

const singleActions = [
	'FIRST_START',
	'TOOLTIP_DISABLED'
];

const stages = ['_REQUEST', '_SUCCESS', '_FAILURE'];

actions.forEach(action => {
  actionTypes[action] = action;
  stages.forEach(stage => {
    const actionType = (action + stage);
    actionTypes[actionType] = actionType;
  });
});

singleActions.forEach(action => {
  actionTypes[action] = action;
});

export default actionTypes;
