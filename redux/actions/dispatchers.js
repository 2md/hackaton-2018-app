import actionTypes from './actionTypes';
import { wooCommerce, wordpress } from '../../services/api';

const dispatchApiRequest = ({
  dispatch,
  url,
  action,
  method = null,
  data = null,
  payload = null,
  additionalPayload = {},
}, service) => {
  const requestConfig = { url };

  if (method) {
    requestConfig.method = method;

    if (data) {
      requestConfig.data = data;
    }
  }

  dispatch({ type: actionTypes[`${action}_REQUEST`], payload, additionalPayload });
  return service(requestConfig)
    .then(res => dispatch({ type: actionTypes[`${action}_SUCCESS`], payload: payload || res.data, additionalPayload  }))
    .catch(err => dispatch({ type: actionTypes[`${action}_FAILURE`], payload: err.response ? err.response.data : null}));
};

export const dispatchWooCommerceRequest = (params) => {
  return dispatchApiRequest(params, wooCommerce);
};

export const dispatchWordpressRequest = (params) => {
	return dispatchApiRequest(params, wordpress);
};
