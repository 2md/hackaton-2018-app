import actionTypes from '../actions/actionTypes'

let dataState = {
	product: null,
	productsMessage: '',
	checkoutMessage: '',
	order: null,
	paymentUrl: '',
	loading: false
}

const checkout = (state = dataState, action) => {
	switch (action.type) {

		case actionTypes.GET_PRODUCTS_REQUEST: {
			return {
				...state,
				loading: true,
				productsMessage: ''
			}
		}
		case actionTypes.GET_PRODUCTS_SUCCESS: {
			return {
				...state,
				loading: false,
				product: action.payload[0],
				productsMessage: ''
			}
		}
		case actionTypes.GET_PRODUCTS_FAILURE: {
			return {
				...state,
				loading: false,
				product: null,
				productsMessage: 'Akcja bilet jest zawieszona!'
			}
		}
		case actionTypes.POST_ORDER_REQUEST: {
			return {
				...state,
				loading: true,
				checkoutMessage: '',
				order: null
			}
		}
		case actionTypes.POST_ORDER_FAILURE: {
			return {
				...state,
				loading: false,
				checkoutMessage: 'Dokonanie płatności nie powiodło się!',
				order: null
			}
		}
		case actionTypes.POST_ORDER_SUCCESS: {
			return {
				...state,
				loading: false,
				checkoutMessage: 'Oczekuje na płatność',
				order: action.payload
			}
		}
		case actionTypes.GET_PAYMENT_URL_REQUEST: {
			return {
				...state,
				loading: true,
				paymentUrl: ''
			}
		}
		case actionTypes.GET_PAYMENT_URL_FAILURE: {
			return {
				...state,
				loading: false,
				paymentUrl: '',
				checkoutMessage: 'Dokonanie płatności nie powiodło się!'
			}
		}
		case actionTypes.GET_PAYMENT_URL_SUCCESS: {
			return {
				...state,
				loading: false,
				paymentUrl: action.payload.redirect
			}
		}
		default:
			return state
	}
}

export default checkout
