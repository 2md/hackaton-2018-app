import {combineReducers} from 'redux';
import layout from './layout.reducer';
import checkout from './checkout.reducer';

export default combineReducers({
	layout,
	checkout
});