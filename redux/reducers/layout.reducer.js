import actionTypes from '../actions/actionTypes'

let dataState = {
	firstStart: true,
	tooltipDisabled: false
};

const layout = (state = dataState, action) => {
	switch (action.type) {
		case actionTypes.FIRST_START: {
			return { ...state, firstStart: false };
		}
		case actionTypes.TOOLTIP_DISABLED: {
			return { ...state, tooltipDisabled: true };
		}
		default:
			return state;
	}
};

export default layout;
