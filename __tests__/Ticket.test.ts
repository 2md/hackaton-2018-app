import 'react-native';
import MockAsyncStorage from 'mock-async-storage';
import { AsyncStorage } from 'react-native';
import {WooCommerce} from "../Client/Service/Client/WooCommerce";
import {Ticket} from "../Client/Service/Ticket";
import {URLSearchParams} from 'url';
import DeviceInfo from 'react-native-device-info';
import {Customer} from "../Client/Model/Customer";
import {WP2MD} from "../Client/Service/Client/WP2MD";
// @ts-ignore
global.fetch = require("node-fetch");
// @ts-ignore
global.URLSearchParams = URLSearchParams;

jest.mock('AsyncStorage', () => new MockAsyncStorage());

describe('Ticket Tests', () => {

    beforeAll((done) => {
        Promise.all([
            WooCommerce.setConfiguration({
                baseURL: 'https://wp.hackaton.2md.pl',
                token: 'Y2tfNTQyYTMwZWQ5MWE0NjE5ZjBmZmIyZjJkNDJlNmRlNGVhMzk2ZmExZjpjc18wNGRiNTVjZjlmMjMzMTYyZTQzMjFmOTQwOTNlY2M1ZTdhZGMyYzZh'
            }),
            Ticket.setCustomer(
                ((customer: Customer) => {
                    customer.addressEmail = 'kwisniowski@goldencomm.com';
                    customer.nickName = 'Johnny Appleseed';

                    return customer;
                })(new Customer())
            ),
            WP2MD.setConfiguration({
                baseURL: 'https://wp.hackaton.2md.pl',
                token: 'Mm1kOlBhc3N3ZA=='
            })
        ]).then(() => {
            done();
        });
    });

    afterAll((done) => {
        Promise.all([
            AsyncStorage.removeItem('2MD::request-woocommerce'),
            AsyncStorage.removeItem('2MD::request-payU')
        ]).then(() => {
            done();
        })
    });

    it('Check the product', () => {
        expect.assertions(1);

        return Ticket.syncActiveProduct().then((product) => {
            return Ticket.getActiveProduct().then((savedProduct) => {
                expect(product).toEqual(savedProduct);
            });
        });
    }, 30*1000);

    it('Mock DeviceInfo', () => {
        expect.assertions(1);

        return DeviceInfo.getIPAddress().then((ip) => {
            expect(ip).toBe('127.0.0.1');
        })
    });

    it('Place order', () => {
        expect.assertions(2);

        return Ticket.placeOrder(10).then((redirectUrl) => {
            expect(redirectUrl).toBeDefined();

            return Ticket.getOrders().then((orders) => {
                expect(orders.length).toBe(1);
            });
        });
    }, 120*1000);

    it('Check if orders succeed', () => {
        expect.assertions(3);

        return Ticket.getOrders().then((orders) => {
            expect(orders.length).toBe(1);

            return fetch(`https://wp.hackaton.2md.pl/wp-json/wc/v3/orders/${orders[0].orderWooCommerce.id}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Basic Y2tfNTQyYTMwZWQ5MWE0NjE5ZjBmZmIyZjJkNDJlNmRlNGVhMzk2ZmExZjpjc18wNGRiNTVjZjlmMjMzMTYyZTQzMjFmOTQwOTNlY2M1ZTdhZGMyYzZh`
                },
                body: JSON.stringify({
                    status: 'completed'
                })
            });
        }).then((response) => {
            expect(response.ok).toBeTruthy();

            return Ticket.getOrdersSucceedForNotification().then((orders) => {
                expect(orders.length).toBe(1);
            });
        });
    }, 120*1000);
});
