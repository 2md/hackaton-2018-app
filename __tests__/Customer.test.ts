import 'react-native';
import {Customer} from "../Client/Model/Customer";

describe('Customer Tests', () => {
    it('Check validator mails', () => {
        const error = new Error('Address Email is not valid');

        let customer = new Customer();
        expect(() => (customer.addressEmail = 'example')).toThrowError(error);
        expect(() => (customer.addressEmail = 'example@')).toThrowError(error);
        expect(() => (customer.addressEmail = 'example@mail')).toThrowError(error);
        expect(() => (customer.addressEmail = 'example@mail.')).toThrowError(error);
        expect(() => (customer.addressEmail = 'example@mail.com')).not.toThrowError(error);
    });
});
