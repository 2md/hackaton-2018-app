import 'react-native';
import MockAsyncStorage from 'mock-async-storage';

const mock = () => {
    const mockImpl = new MockAsyncStorage();
    jest.mock('AsyncStorage', () => mockImpl);
};

mock();

import {AsyncStorage as storage} from 'react-native';

it('Mock Async Storage working', async () => {
    await storage.setItem('myKey', 'myValue');
    const value = await storage.getItem('myKey');
    expect(value).toBe('myValue');
});