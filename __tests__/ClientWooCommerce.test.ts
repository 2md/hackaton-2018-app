import 'react-native';
import { AsyncStorage } from 'react-native';
import {WooCommerce} from "../Client/Service/Client/WooCommerce";
import MockAsyncStorage from 'mock-async-storage';
// @ts-ignore
global.fetch = require("node-fetch");

const mock = () => {
    const mockImpl = new MockAsyncStorage();
    jest.mock('AsyncStorage', () => mockImpl);
};

const release = () => jest.unmock('AsyncStorage');

describe('WooCommerceClient Tests', () => {
    beforeAll((done) => {
        mock();

        WooCommerce.setConfiguration({
            baseURL: 'https://wp.hackaton.2md.pl',
            token: 'Y2tfNTk2ZmIwNTIzNDgyYjE1NGFhY2QwNmI4NjA2OGRkMzE0N2ZkNTNmYzpjc182MDJhY2I5NmNlNGQ0NzZlNGUwOTcxMGFjODgwZmM2MDgzY2UzZDZk'
        }).then(() => {
            done();
        });
    });

    afterAll((done) => {
        AsyncStorage.removeItem('2MD::request-woocommerce').then(() => {
            done();
            release();
        });
    });

    it('make request with fetch API', (done) => {
        fetch('https://postman-echo.com/get', {
            method: 'GET'
        }).then((response) => {
            expect(response.ok).toBe(true);
            done();
        });
    }, 2*60*1000);

    it ('checking the basic request', () => {
        expect.assertions(1);

        return WooCommerce.getMethods().then((response) => {
            expect(response).toBeDefined();
        });
    }, 10*1000);
});
