import isEmail from 'validator/lib/isEmail';

export const isEmailAddress = str => isEmail(str);
