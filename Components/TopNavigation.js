import React from 'react'
import { TouchableOpacity, StyleSheet, View, Platform, Text } from 'react-native'
import { withNavigation } from 'react-navigation'
import * as Icon from '@expo/vector-icons'

class TopNavigation extends React.Component {

	render () {
		return (
			<View
				style={styles.shadow}>
				<TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}
													style={{
														flex:1,
													paddingLeft: 20}}>
					<Icon.Ionicons
						name={Platform.OS === 'ios' ? 'ios-arrow-back' : 'md-arrow-back'}
						size={30}
						color="#fff"
					/>
				</TouchableOpacity>
				<Text style={{flex:4, color: '#fff', textAlign: 'center', fontWeight: 'bold', fontSize: 20}}>{this.props.title}</Text>
				<View style={{flex:1,
				paddingRight: 20}}></View>
			</View>
		)
	}
}

export default withNavigation(TopNavigation)

const styles = StyleSheet.create({
	shadow: {
		height: 90,
		paddingTop: 50,
		flexDirection: 'row',
		justifyContent: 'space-between',
	}

})
