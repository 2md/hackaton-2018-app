import React from 'react'
import { TouchableOpacity, StyleSheet, View, Text } from 'react-native'
import { withNavigation } from 'react-navigation'
import * as Icon from '@expo/vector-icons'
import { connect } from 'react-redux'
import { getTickets } from '../redux/actions/tickets'

class Footer extends React.Component {

	componentWillMount() {
		this.props.dispatch(getTickets())
	}

	render () {
		const {navigate} = this.props.navigation
		const {isVisible, product, productsMessage } = this.props

		if (isVisible) {
			return (
				<View style={{flex: 2, justifyContent: 'center',}}>
					<Text style={styles.helpText}>
						<Text style={{fontWeight: 'bold'}}>Kliknij </Text>
						i wejdz do naszej aplikacji</Text>
				</View>
			)
		} else {
			if (product) {
				return (
					<View style={{padding: 20, paddingBottom: 50}}>
						<Text style={[styles.statisticText, {marginBottom: 40}]}>
							<Icon.Entypo
								name='ticket'
								size={32}
								style={{marginBottom: -3, paddingRight: 50}}
								color="#fff"
							/>
							<Text style={{fontWeight: 'bold'}}> Podarowano: </Text> {product.total_sales} biletow
						</Text>

						<Text style={[styles.helpText, {marginBottom: 20}]}>Podaruj usmiech - to tylko
							<Text style={{fontWeight: 'bold'}}> {product.price} zł</Text>
						</Text>
						<TouchableOpacity
							style={styles.buyButton}
							onPress={() => navigate('Checkout')}>
							<Text style={styles.buyButtonText}>Podaruj Bilet</Text>
						</TouchableOpacity>

					</View>
				)
			} else {
				return (
					<View style={{padding: 20, paddingBottom: 50}}>
						<Text style={[styles.statisticText, {marginBottom: 40}]}>
							<Text style={{fontWeight: 'bold'}}>{productsMessage}</Text>
						</Text>
					</View>
				)
			}
		}
	}
}


const mapStateToProps = (state) => ({
	product: state.checkout.product,
	productsMessage: state.checkout.productsMessage
})

const FooterNav = withNavigation(Footer);

export default connect(mapStateToProps)(FooterNav)



const styles = StyleSheet.create({
	statisticText: {
		textAlign: 'center',
		color: '#fff',
		fontSize: 18,
	},
	helpText: {
		textAlign: 'center',
		color: '#557a92'
	},
	buyButton: {
		backgroundColor: '#e64f18',
		borderRadius: 6,
		shadowOpacity: 0.5,
		shadowRadius: 5,
		shadowColor: 'rgba(0,0,0,0.3)',
		shadowOffset: {height: 10, width: 0},
	},
	buyButtonText: {
		textAlign: 'center',
		color: '#fff',
		fontSize: 20,
		padding: 15
	}
})
