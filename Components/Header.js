import React from 'react'
import { TouchableOpacity, StyleSheet, View, Platform, Share, Text, Animated, Easing } from 'react-native'
import { withNavigation } from 'react-navigation'
import * as Icon from '@expo/vector-icons'

class Header extends React.Component {
	state = {
		infoTooltipFade: new Animated.Value(0),
		ShareTooltipFade: new Animated.Value(0),
	}

	componentDidMount () {
		if (this.props.isFirstStart) {
			this.tooltipAnimation()
		}
	};

	tooltipAnimation () {
		Animated.sequence([
			Animated.timing(this.state.infoTooltipFade, {
				toValue: 1,
				easing: Easing.linear,
				delay: 5000,
				duration: 400
			}),
			Animated.timing(this.state.infoTooltipFade, {
				toValue: 0,
				easing: Easing.linear,
				delay: 2000,
				duration: 400
			}),
			Animated.timing(this.state.ShareTooltipFade, {
				toValue: 1,
				easing: Easing.linear,
				delay: 1000,
				duration: 400
			}),
			Animated.timing(this.state.ShareTooltipFade, {
				toValue: 0,
				easing: Easing.linear,
				delay: 2000,
				duration: 400
			})
		]).start()
	}


	onShare = () => {
		Share.share({
			message: 'Hackaton',
			url: 'https://www.facebook.com/code4cause.net',
			title: 'KupBilet'
		}, {
			// Android only:
			dialogTitle: 'Pochwal się aplikacją KupBilet',
			// iOS only:
			excludedActivityTypes: [
				'com.apple.UIKit.activity.PostToTwitter'
			]
		})
	}

	render () {
		if (this.props.isFirstStart) { return null }
		return (
			<View>
				<TouchableOpacity
					style={[styles.topButton, {left: 20}]}
					onPress={() => this.props.navigation.navigate('Information')}>
					<Icon.Ionicons
						name={Platform.OS === 'ios' ? 'ios-information-circle' : 'md-information-circle'}
						size={30}
						color="#fff"
					/>
				</TouchableOpacity>
				<Animated.View style={[styles.tooltip, {left: 60, opacity: this.state.infoTooltipFade}]}>
					<View style={[
						styles.tooltipArrow,
						{left: -8, transform: [{rotate: '-90deg'}]}
					]}></View>
					<Text style={styles.tooltipText}>Informacje o Akcji</Text>
				</Animated.View>

				<TouchableOpacity
					style={[styles.topButton, {right: 0}]}
					onPress={this.onShare}>
					<Icon.Ionicons
						name={Platform.OS === 'ios' ? 'ios-share' : 'md-share'}
						size={26}
						color="#fff"
					/>
				</TouchableOpacity>
				<Animated.View style={[styles.tooltip, {right: 65, opacity: this.state.ShareTooltipFade}]}>
					<View style={[
						styles.tooltipArrow,
						{right: -8, transform: [{rotate: '90deg'}]}
					]}></View>
					<Text style={styles.tooltipText}>Udostępnij aplikacje</Text>
				</Animated.View>

			</View>
		)
	}
}

export default withNavigation(Header)

const styles = StyleSheet.create({
	topButton: {
		position: 'absolute',
		zIndex: 200,
		height: 50,
		width: 50,
		top: 50,
	},
	tooltip: {
		position: 'absolute',
		zIndex: 200,
		paddingLeft: 12,
		paddingRight: 12,
		paddingTop: 8,
		paddingBottom: 8,
		top: 48,
		backgroundColor: 'rgba(0,0,0,0.3)',
		borderRadius: 5
	},
	tooltipText: {
		color: '#fff'
	},
	tooltipArrow: {
		position: 'absolute',
		top: '70%',
		width: 0,
		height: 0,
		backgroundColor: 'transparent',
		borderStyle: 'solid',
		borderLeftWidth: 4,
		borderRightWidth: 4,
		borderBottomWidth: 8,
		borderLeftColor: 'transparent',
		borderRightColor: 'transparent',
		borderBottomColor: 'rgba(0,0,0,0.3)'
	}
})
