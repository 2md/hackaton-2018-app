import React from 'react'
import { Animated, Image, StyleSheet, Easing, View, TouchableOpacity } from 'react-native'
import Dialogs from '../../Components/Animations/Dialogs'

export default class Kids extends React.Component {
	state = {
		xc: false,
		position: new Animated.ValueXY({
			x: this.props.isFirstStart ? 500 : 0,
			y: -50
		}),
	}

	startScreen () {
		Animated.timing(this.state.position, {
			toValue: {x: 0, y: -50},
			easing: Easing.bezier(0, 0, .58, 1),
			duration: 200
		}).start()
	}

	render () {
		return (
			<Animated.View style={[
				styles.kids, {
					transform: [
						{translateX: this.state.position.x},
						{translateY: this.state.position.y}
					]
				}
			]}>
				<TouchableOpacity onPress={this.startScreen}
													disabled={!this.state.isAnimating}
													activeOpacity={1}>
					<View>
						<Image style={styles.image}
									 source={require('../../Assets/images/kid-3.png')}
									 resizeMode="contain" />
					</View>
				</TouchableOpacity>
				<TouchableOpacity onPress={this.startScreen}
													disabled={!this.state.isAnimating}
													activeOpacity={1}>
					<View>
						<Image style={styles.image}
									 source={require('../../Assets/images/kid-2.png')}
									 resizeMode="contain" />
					</View>
				</TouchableOpacity>
				<Dialogs/>
			</Animated.View>
		)
	}
}

const styles = StyleSheet.create({
	kids: {
		width: '100%',
		flexDirection: 'row',
		justifyContent: 'center',
	},
	image: {
		height: 130,
		width: 150,
	},
})
