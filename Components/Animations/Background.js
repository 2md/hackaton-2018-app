import React from 'react'
import { Animated, StyleSheet, Easing } from 'react-native'

export default class Background extends React.Component {
	state = {
		position: new Animated.ValueXY({
			x: 0,
			y: -80
		})
	}

	startScreen () {
		let {position} = this.state
		Animated.sequence([
			Animated.timing(position, {
				toValue: {x: -250, y: -160},
				easing: Easing.bezier(1, 0, 1, 1),
				duration: 200
			}),
			Animated.timing(position, {
				toValue: {x: 300, y: -160},
				duration: 1
			}),
			Animated.timing(position, {
				toValue: {x: 0, y: -105},
				easing: Easing.bezier(0, 0, .58, 1),
				duration: 200
			}),
		]).start()
	}

	render () {
		let {position} = this.state
		return (
			<Animated.View style={[
				styles.background, {
					transform: [
						{translateX: position.x},
						{translateY: position.y},
						{scaleX: 1.70}
					]
				}
			]}/>
		)
	}
}
const styles = StyleSheet.create({
	background: {
		position: 'absolute',
		bottom: -160,
		left: 120,
		backgroundColor: '#fff',
		alignSelf: 'center',
		width: 180,
		height: 180,
		borderRadius: 90,
	},
})



