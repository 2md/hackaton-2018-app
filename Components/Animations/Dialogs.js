import React from 'react'
import { Animated, Image, StyleSheet, Easing, Text, View } from 'react-native'

export default class Dialogs extends React.Component {
	state = {
		opacity: new Animated.Value(0),
		scale: {
			x: 1,
			y: 1
		},
		position: {
			x: -5,
			y: 0
		},
		person: '',
		persons: [
			'boy',
			'girl'
		],
		dialog: '',
		dialogs: [
			'Kup mi bilet do kina!',
			'Idziemy do Kina?',
			'Ja chcę na Awengersów!',
			'Nudzi mi się',
			'Tra la la la... la',
			'Co robimy?'
		],
		intervalId: null
	}

	componentDidMount () {
		let {dialogs, opacity, persons} = this.state
		this.interval = setInterval(() => {
			this.setState({
				dialog: dialogs[Math.floor(Math.random() * dialogs.length )],
				person: persons[Math.floor(Math.random() * persons.length )]
			}, () => {
				Animated.sequence([
					Animated.timing(opacity, {
						toValue: 1,
						easing: Easing.bezier(.42, 0, .58, 1),
						duration: 300
					}),
					Animated.timing(opacity, {
						toValue: 0,
						easing: Easing.bezier(.42, 0, .58, 1),
						delay: 6000,
						duration: 300
					})
				]).start()
			})
		}, 12000);
	}

	componentWillUnmount(){
		clearInterval(this.interval);
	}

	render () {
		let {opacity, position, dialog, person} = this.state
		return (
			<Animated.View style={[
				styles.dialog, {
					opacity: opacity,
					transform: [
						{translateX: position.x},
						{translateY: position.y}
					]
				}
			]}>
				<Image style={[styles.image, {
					transform: [
						{scaleX: person === 'boy' ? -1 : 1},
						{scaleY: 1}
					]
				}]}
							 source={require('../../Assets/images/dialog.png')}
							 resizeMode="contain"
				/>
				<View style={{
					position: 'absolute',
					height:100,
					top:0,
					width: 150,
					padding: 5,
					justifyContent: 'center',
				}}>
					<Text style={{
						fontWeight: 'bold',
						fontSize:16,
						textAlign: 'center'
					}}>{dialog}</Text>
				</View>

			</Animated.View>
		)
	}
}

const styles = StyleSheet.create({
	dialog: {
		position: 'absolute',
		bottom: 80,
	},
	image: {
		height: 130,
		width: 150,
	},
})
