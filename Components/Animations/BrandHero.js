import React from 'react'
import { Animated, Image, StyleSheet, Easing, TouchableOpacity, View } from 'react-native'

export default class BrandHero extends React.Component {
	state = {
		isAnimating: false,
		scale: new Animated.Value(this.props.isFirstStart ? 1 : 0.75),
		position: new Animated.ValueXY({
			x: 10,
			y: this.props.isFirstStart ? 10 : 20
		}),
	}

	startScreen () {
		Animated.sequence([
			Animated.parallel([
				Animated.timing(this.state.position, {
					toValue: {x: 10, y: -160},
					easing: Easing.bezier(.25, .1, .25, 1),
					duration: 300
				}),
				Animated.timing(this.state.scale, {
					toValue: 0.7,
					easing: Easing.bezier(.25, .1, .25, 1),
					duration: 300
				})
			]),
			Animated.parallel([
				Animated.timing(this.state.position, {
					toValue: {x: 10, y: 20},
					easing: Easing.bezier(.42, 0, .58, 1),
					duration: 200
				}),
				Animated.timing(this.state.scale, {
					toValue: 0.75,
					easing: Easing.bezier(.42, 0, .58, 1),
					duration: 200
				})
			])
		]).start()
	}
	jumpAnimation () {
		Animated.sequence([
				Animated.timing(this.state.position, {
					toValue: {x: 10, y: -160},
					easing: Easing.bezier(.25, .1, .25, 1),
					duration: 300
				}),

				Animated.timing(this.state.position, {
					toValue: {x: 10, y: 20},
					easing: Easing.bezier(.42, 0, .58, 1),
					duration: 200
				})
		]).start()
	}

	render () {
		return (
			<Animated.View style={[
				styles.hero, {
					transform: [
						{translateX: this.state.position.x},
						{translateY: this.state.position.y},
						{scaleX: this.state.scale},
						{scaleY: this.state.scale}
					]
				}
			]}>

					<Image style={styles.image}
								 source={require('../../Assets/images/kid-1.png')}
								 resizeMode="contain"
					/>
			</Animated.View>
		)
	}
}

const styles = StyleSheet.create({
	hero: {
		width: '100%',
		flexDirection: 'row',
		justifyContent: 'center',
		backgroundColor: '#ccc'
	},
	image: {
		position: 'absolute',
		bottom: 0,
		height: 190,
		width: 190,

	},
	touchable: {
		height: '100%',
		width: '100%',
		backgroundColor: '#000'

	}
})
