const DeviceInfo = jest.genMockFromModule('react-native-device-info');

// @ts-ignore
DeviceInfo.getIPAddress = () => Promise.resolve('127.0.0.1');

export default DeviceInfo;