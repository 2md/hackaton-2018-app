import React from 'react';
import { Animated, Image, StyleSheet, View, } from 'react-native'

export default class Logo extends React.Component {

  render() {
			return (
				<Animated.View style={{flex: 3}}>
					<Image style={styles.logoImage}
								 source={require('../Assets/images/logo.png')}
								 resizeMode="contain"
					/>
				</Animated.View>
			)

  }
}
const styles = StyleSheet.create({

	logoImage: {
		flex: 1,
		alignSelf: 'center',
		height: 150,
		width: 300,
	},
})
