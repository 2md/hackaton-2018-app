import { AsyncStorage } from 'react-native';

export class Customer {
    private _addressEmail?: string;
    private _nickName?: string;
    private readonly emailRegEx: RegExp;

    constructor() {
        this.emailRegEx = new RegExp("(?:[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+\\=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])");
    }

    get addressEmail(): string {
        return this._addressEmail;
    }

    /**
     * @param value
     */
    set addressEmail(value: string) {
        if (!this.emailRegEx.test(value)) {
            throw new Error('Address Email is not valid');
        }

        this._addressEmail = value;
    }

    get nickName(): string {
        return this._nickName;
    }

    set nickName(value: string) {
        this._nickName = value;
    }
}
