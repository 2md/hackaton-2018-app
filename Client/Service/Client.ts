import { AsyncStorage } from "react-native";
import {Cryptor} from "./Cryptor";

export abstract class Client {

    protected static get keyRequest(): string {
        throw new Error('Method must be override');
    }


    static setConfiguration(conf): Promise<any> {
        return this.getRequest().catch((reason: Error) => {
            if (reason.message == 'Request is not set') {
                return this.setRequest(conf);
            }

            throw reason;
        });
    }

    protected static getRequest(): Promise<any> {
        return AsyncStorage.getItem(this.keyRequest).then((value) => {
            if (!value) {
                throw new Error('Request is not set');
            }

            return Cryptor.unencrypt(value);
        });
    }

    protected static setRequest(value: any): Promise<any> {
        return AsyncStorage.setItem(this.keyRequest, Cryptor.encrypt(value));
    }

    protected static responseToJson(response: Response): Promise<any> {
        if (!response.ok) {
            return response.text().then((text) => {
                throw new Error(text);
            });
        }

        return response.json();
    }
}
