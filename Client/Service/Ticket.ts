import { AsyncStorage } from "react-native";
import {Cryptor} from "./Cryptor";
import {WooCommerce} from "./Client/WooCommerce";
import {Customer} from "../Model/Customer";
import {Order} from "../Model/Order";
import DeviceInfo from 'react-native-device-info';
import {WP2MD} from "./Client/WP2MD";

const keyActiveProductId = '2MD::active-ticker-product-id';
const keyCustomer = '2MD::customer-ticket';
const keyOrders = '2MD::orders-ticket';
const merchantPosId = '345670';

export class Ticket {
    static getActiveProduct(): Promise<any> {
        return AsyncStorage.getItem(keyActiveProductId).then(item => Cryptor.unencrypt(item));
    }

    static setActiveProduct(item: any) {
        return AsyncStorage.setItem(keyActiveProductId, Cryptor.encrypt(item));
    }

    static syncActiveProduct(): Promise<any> {
        const searchParams = new URLSearchParams();
        searchParams.append('status', 'publish');
        searchParams.append('order', 'asc');
        searchParams.append('type', 'simple');
        searchParams.append('per_page', '1');

        return WooCommerce.getProducts(searchParams).then(items => {
            if (!items.length) {
                throw new Error('No Active Ticket in this moment');
            }

            return Ticket.setActiveProduct(items[0]).then(() => items[0]);
        });
    }

    static getCustomer(): Promise<Customer> {
        return AsyncStorage.getItem(keyCustomer).then(item => {
            if (item == null) {
                throw new Error('Customer is not defined');
            }

            return ((item) => {
                const customer = new Customer();
                customer.addressEmail = item._addressEmail;
                customer.nickName = item._nickName;

                return customer;
            })(Cryptor.unencrypt(item));
        });
    }

    static setCustomer(item: Customer): Promise<any> {
        return AsyncStorage.setItem(keyCustomer, Cryptor.encrypt(item));
    }

    static getOrders(): Promise<[Order]> {
        return AsyncStorage.getItem(keyOrders).then(item => {
            if (item == null) {
                return [];
            }

            return Cryptor.unencrypt(item);
        });
    }

    static setOrders(items: [Order]): Promise<any> {
        return AsyncStorage.setItem(keyOrders, Cryptor.encrypt(items));
    }

    static addOrder(item: Order): Promise<any> {
        return Ticket.getOrders().then(items => {
            items.push(item);

            return Ticket.setOrders(items);
        });
    }

    /**
     * Order tickets with given amount
     * @param int amount
     */
    static placeOrder(amount: number): Promise<string> {
        const order = new Order();

        let product: any;
        let customer: Customer;
        let ip: string;

        if (amount <= 0) {
            throw new Error('Amount must be higher then zero');
        }

        return Promise.all([
            Ticket.getActiveProduct(),
            Ticket.getCustomer(),
            DeviceInfo.getIPAddress()
        ]).then((values) => {
            product = values[0];
            customer = values[1] as Customer;
            ip = values[2];

            if (product === null || customer === null) {
                throw new Error('Product or Customer are not set');
            } else if (product.id === undefined) {
                throw new Error('Product does not have id');
            } else if (customer.addressEmail === undefined) {
                throw new Error('Customer email is not set');
            }

            return WooCommerce.postOrder({
                "payment_method": "payu",
                "set_paid": false,
                "billing": {
                    "email": "john.doe@example.com",
                },
                "line_items": [
                    {
                        "product_id": product.id,
                        "quantity": amount
                    }
                ],
                "shipping_lines": [
                    {
                        "method_id": "flat_rate",
                        "method_title": "Flat Rate",
                        "total": "0"
                    }
                ]
            });
        }).then((infoOrder) => {
            order.orderWooCommerce = infoOrder;

            return Ticket.addOrder(order);
        }).then(() => {
            return WP2MD.processPayment(order.orderWooCommerce.id);
        });
    }

    static getOrdersSucceedForNotification(): Promise<Order[]> {
        return Ticket.getOrders().then((orders) => {
            return orders.filter(order => !order.isSuccessNotified);
        }).then((orders) => {
            return WP2MD.getOrdersFilterWithStatus(orders, WP2MD.STATUS_COMPLETED);
        }).then((orders) => {
            if (orders.length === 0) {
                return [];
            }

            return Ticket.getOrders().then((localOrders) => {
                for (let key in orders) {
                    let index: number = -1;
                    for (let finder in localOrders) {
                        if (localOrders[finder].orderWooCommerce.id == orders[key].orderWooCommerce.id) {
                            index = Number(finder);
                        }
                    }

                    if (index === -1) {
                        throw new Error('OrderForNotification is not saved locally');
                    }

                    localOrders[index].isSuccessNotified = true;
                }

                return Ticket.setOrders(localOrders).then(() => (orders));
            });
        });
    }
}
