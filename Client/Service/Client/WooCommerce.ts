/**
 * @copyright 2MD
 */
import {Client} from "../Client";

// @ts-ignore
export class WooCommerce extends Client {

    protected static get keyRequest(): string {
        return '2MD::request-woocommerce';
    }

    static setConfiguration(conf): Promise<any> {
        return super.setConfiguration({
            url: `${conf.baseURL}/wp-json/wc/v3`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Basic ${conf.token}`
            },
        });
    }

    static getMethods() {
        return WooCommerce.getRequest().then(request => {
            return fetch(request.url, {
                method: 'GET',
                headers: request.headers
            })
        }).then(WooCommerce.responseToJson);
    }

    static getProducts(payload: URLSearchParams): Promise<any> {
        return WooCommerce.getRequest().then((request) => {
            return fetch(`${request.url}/products?${payload.toString()}`, {
                method: 'GET',
                headers: request.headers
            });
        }).then(WooCommerce.responseToJson);
    }

    static postOrder(payload: object) {
        return WooCommerce.getRequest().then(request => {
            return fetch(`${request.url}/orders`, {
                method: 'POST',
                headers: request.headers,
                body: JSON.stringify(payload)
            });
        }).then(WooCommerce.responseToJson);
    }
}
