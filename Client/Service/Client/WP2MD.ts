import {Client} from "../Client";
import {Order} from "../../Model/Order";

export class WP2MD extends Client {
    static readonly STATUS_COMPLETED: string = 'COMPLETED';

    protected static get keyRequest(): string {
        return '2MD::request-wp2md';
    }

    static setConfiguration(conf): Promise<any> {
        return super.setConfiguration({
            url: `${conf.baseURL}/wp-json/wp2md/v1`,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Basic ${conf.token}`
            }
        });
    }

    static processPayment(orderId: number): Promise<string> {
        return this.getRequest().then((request) => {
            return fetch(`${request.url}/process-payment-order/${orderId}`, {
                method: 'GET',
                headers: request.headers
            });
        }).then(this.responseToJson).then((response) => {
            if (response.result !== 'success') {
                throw new Error(response);
            }

            return response.redirect
        });
    }

    static getOrdersFilterWithStatus(orders: Order[], status: string): Promise<Order[]> {
        if (orders.length === 0) {
            return Promise.resolve([]);
        }

        return this.getRequest().then((request) => {
            return fetch(`${request.url}/orders-filter-with-status/${status}`, {
                method: 'POST',
                headers: request.headers,
                body: JSON.stringify({
                    orders: orders.map((order) => (order.orderWooCommerce.id as number))
                })
            });
        }).then(this.responseToJson).then((response: number[]) => {
            return orders.filter((order) => (response.indexOf(order.orderWooCommerce.id) !== -1));
        });
    }
}
