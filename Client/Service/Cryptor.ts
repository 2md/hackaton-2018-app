import * as CryptoJS from "crypto-js";

const CryptKey = '\\3_lc{EictEskLBGBy6;BaM[8Wak9(s';

export class Cryptor {
    public static encrypt(value: any): string {
        return CryptoJS.AES.encrypt(JSON.stringify(value), CryptKey).toString();
    }

    public static unencrypt(value: string): any {
        return JSON.parse(CryptoJS.AES.decrypt(value, CryptKey).toString(CryptoJS.enc.Utf8));
    }
}