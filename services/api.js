import axios from 'axios';
import getEnvVars from '../environment';

const { WcUrl, WcToken, WpUrl, WpToken } = getEnvVars();
const DEFAULT_OPTIONS = {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json; charset=utf-8'
  }
};

const wooCommerceService = axios.create({
  baseURL: WcUrl,
  withCredentials: false,
	dataType: 'json',
  headers: {
		Authorization: 'Basic ' + WcToken
  }
});
const wordpressService = axios.create({
	baseURL: WpUrl,
	withCredentials: false,
	dataType: 'json',
	headers: {
		Authorization: 'Basic ' + WpToken
	}
});

export function wooCommerce (config) {
  const options = Object.assign({}, DEFAULT_OPTIONS, config);
  return wooCommerceService.request(options);
}



export function wordpress (config) {
	const options = Object.assign({}, DEFAULT_OPTIONS, config);
	return wordpressService.request(options);
}
