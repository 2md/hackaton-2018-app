import { Constants } from "expo";
/*
import { Platform } from "react-native";
const localhost =
	Platform.OS === "ios" ? "localhost:8080" : "10.0.2.2:8080";
*/

const ENV = {
	dev: {
		WpUrl: 'https://wp.hackaton.2md.pl/wp-json/wp2md/v1/',
		WpToken: 'Mm1kOlBhc3MybWQxMSE=',
		WcUrl: 'https://wp.hackaton.2md.pl/wp-json/wc/v3/',
		WcToken: 'Y2tfMzUzNDAxODQwNzg0NmMxZTZlZDFhMDMzNDY1NzE3ODUwNjBjZDE4MDpjc18zNWQ5OTA2NzNkYTlhMDU1YTg2NzE0NGI0OWI2OGUxNTgzMDlkZDUw',
		NODE_TLS_REJECT_UNAUTHORIZED: '0'
	},
	staging: {
		WpUrl: 'https://wp.hackaton.2md.pl/wp-json/wp2md/v1/',
		WpToken: 'Mm1kOlBhc3MybWQxMSE=',
		WcUrl: 'https://wp.hackaton.2md.pl/wp-json/wc/v3/',
		WcToken: 'Y2tfMzUzNDAxODQwNzg0NmMxZTZlZDFhMDMzNDY1NzE3ODUwNjBjZDE4MDpjc18zNWQ5OTA2NzNkYTlhMDU1YTg2NzE0NGI0OWI2OGUxNTgzMDlkZDUw',
		NODE_TLS_REJECT_UNAUTHORIZED: '0'
	},
	prod: {
		WpUrl: 'https://wp.hackaton.2md.pl/wp-json/wp2md/v1/',
		WpToken: 'Mm1kOlBhc3MybWQxMSE=',
		WcUrl: 'https://wp.hackaton.2md.pl/wp-json/wc/v3/',
		WcToken: 'Y2tfMzUzNDAxODQwNzg0NmMxZTZlZDFhMDMzNDY1NzE3ODUwNjBjZDE4MDpjc18zNWQ5OTA2NzNkYTlhMDU1YTg2NzE0NGI0OWI2OGUxNTgzMDlkZDUw',
		NODE_TLS_REJECT_UNAUTHORIZED: '0'
	}
};

const getEnvVars = (env = Constants.manifest.releaseChannel) => {
	if (__DEV__) {
		return ENV.dev;
	} else if (env === 'staging') {
		return ENV.staging;
	} else if (env === 'prod') {
		return ENV.prod;
	}
};

export default getEnvVars;
