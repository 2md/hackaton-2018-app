import React from 'react';
import { createSwitchNavigator } from 'react-navigation';

import Home from '../Views/Home';
import Checkout from '../Views/Checkout';
import Information from '../Views/Information';
import ThankYouPage from '../Views/ThankYouPage';

export default createSwitchNavigator({
	Home: {screen: Home},
	Information: {screen: Information},
	Checkout: {screen: Checkout},
	ThankYouPage: {screen: ThankYouPage}
});
