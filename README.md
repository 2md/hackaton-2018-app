# HACKATHON: vol.1

## Cel akcji: 

> Stworzenie innowacyjnej aplikacji mobilnej, dzięki której wesprzemy "AKCJĘ BILET" i spełnimy filmowe marzenia Dzieci z wrocławskich domów dziecka. Aplikacja wykonana będzie w technologii React Native.

## Documentation (setup)

1. Download and install the latest version of Node
1. clone the repository
1. npm install expo-cli --global
1. npm install
1. expo start
1. Install Expo Client on your device
1. Scan QR Code


## Links

* https://expo.io/learn
* https://docs.nativebase.io/docs/GetStarted.html
* https://play.google.com/store/apps/details?id=host.exp.exponent&referrer=www