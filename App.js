import React from 'react'
import { Platform, StatusBar, StyleSheet, UIManager, View } from 'react-native'
import { AppLoading } from 'expo'
import * as Font from 'expo-font';
import * as Icon from '@expo/vector-icons';
import { Asset } from 'expo-asset';

import AppNavigator from './navigation/AppNavigator'
import { initStore } from './redux/store'
import { Provider } from 'react-redux'

const store = initStore()

export default class App extends React.Component {
	state = {
		isLoadingComplete: false,
		firstStart: true,
	}

	constructor () {
		super()
		UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true)
	}

	render () {
		if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
			return (
				<AppLoading
					startAsync={this._loadResourcesAsync}
					onError={this._handleLoadingError}
					onFinish={this._handleFinishLoading}
				/>
			)
		} else {
			return (
				<Provider store={store}>
					<View style={styles.container}>
						{Platform.OS === 'ios' && <StatusBar barStyle="default"/>}
						<AppNavigator/>
					</View>
				</Provider>
			)
		}
	}

	_loadResourcesAsync = async () => {
		return Promise.all([
			Asset.loadAsync([
				require('./Assets/images/logo.png'),
				require('./Assets/images/kid-1.png'),
				require('./Assets/images/kid-2.png'),
				require('./Assets/images/kid-3.png'),
			]),
			Font.loadAsync({
				...Icon.Ionicons.font,
				'space-mono': require('./Assets/fonts/SpaceMono-Regular.ttf'),
				'Ionicons': require('@expo/vector-icons/build/vendor/react-native-vector-icons/Fonts/Ionicons.ttf'),
				'Entypo': require('@expo/vector-icons/build/vendor/react-native-vector-icons/Fonts/Entypo.ttf'),
			})
		])
	}

	_handleLoadingError = error => {
		console.warn(error)
	}

	_handleFinishLoading = () => {
		this.setState({isLoadingComplete: true})
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#07263a'
	},
})
